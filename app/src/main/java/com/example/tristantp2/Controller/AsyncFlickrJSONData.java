package com.example.tristantp2.Controller;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.tristantp2.Utils.Constants.*;
import static com.example.tristantp2.Helper.Helper.*;

/**
 * This class is used to get the first image of the answer given by the Flickr API.
 * The link of the image will be send to another class to be downloaded and displayed.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class AsyncFlickrJSONData extends AsyncTask<String,Void, JSONObject> {

    private ImageView img_getFlickr;

    /**
     * Constructor of AsyncFlickrJSONData
     * @param img_getFlickr ImageView that we want to update/set.
     */
    public AsyncFlickrJSONData(ImageView img_getFlickr) {
        this.img_getFlickr = img_getFlickr;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        JSONObject objJ = null;
        URL url = null;
        try {
            url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String s = ReadStream(in);
                objJ = new JSONObject(s);
            } catch (JSONException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATA,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATA,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATA,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        }
        return objJ;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        try {
            JSONObject objectImg = getImage(jsonObject.getJSONArray(JSONTOKEN_ITEMS),0);
            new AsyncBitmapDownloader(img_getFlickr).execute(objectImg.getJSONObject(JSONTOKEN_MEDIA).getString(JSONTOKEN_URL));
        } catch (JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATA,TAG_METFUNC_ONPOSTEXECUTE), e.getMessage(), e);
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(JSONObject jsonObject) {
        super.onCancelled(jsonObject);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    /**
     * This function to return an image at a particular index.
     *
     * @param pictures it represents the array of images
     * @param index it is the index of the image that we want to get
     * @return JSONObject return the object at the particular index
     */
    private JSONObject getImage(JSONArray pictures,int index) throws JSONException {
        return pictures.getJSONObject(index);
    }
}

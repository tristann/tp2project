package com.example.tristantp2.Controller;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.tristantp2.Utils.Constants.*;

/**
 * This class is used to transform a link of an image to a bitmap.
 * The bitmap will be displayed after the process ended.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class AsyncBitmapDownloader extends AsyncTask<String,Void, Bitmap> {

    private ImageView img_getFlickr;

    /**
     * Constructor of AsyncBitmapDownloader
     * @param img_getFlickr ImageView that we want to update/set.
     */
    public AsyncBitmapDownloader(ImageView img_getFlickr) {
        this.img_getFlickr = img_getFlickr;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        Bitmap res = null;
        URL url = null;
        try {
            url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                res = BitmapFactory.decodeStream(in);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCBITMAPDOWNLOADER,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCBITMAPDOWNLOADER,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        }
        return res;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        img_getFlickr.setImageBitmap(bitmap);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(Bitmap bitmap) {
        super.onCancelled(bitmap);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}

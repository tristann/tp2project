package com.example.tristantp2.Utils;

/**
 * This class is used to stored all the constant of the program.
 * If you modify one value here all the program will change.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class Constants {
    /*GENERAL*/
    private static final String PROPERTIES_API_KEY = "4167810fbc47cc7f1da6d151edf224d5";
    public static final String PROPERTIES_API_URL = "https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json&nojsoncallback=1";
    public static final String PROPERTIES_API_URL_ADVANCED = String.format("https://api.flickr.com/services/rest/?method=flickr.photos.search&license=4&api_key=%s&privacy_filter=1&format=json&nojsoncallback=1&per_page=1&has_geo=1",PROPERTIES_API_KEY);
    public static final String PROPERTIES_API_URL_PHOTO = "https://live.staticflickr.com/";
    public static final String PROPERTIES_API_URL_AUHT = "https://httpbin.org/basic-auth/bob/sympa";
    public static final String PROPERTIES_CASE_AUTH = "auth";
    public static final String PROPERTIES_CASE_FLICKR = "flickr";
    public static final String PROPERTIES_EMPTY_STRING = "";
    public static final String PROPERTIES_DEFAULT_STRING = "UNKNOW";
    public static final String PROPERTIES_THREAD_THREADCUSTOM = "THREADCUSTOM";
    public static final String PROPERTIES_FALSE= "false";
    public static final String PROPERTIES_AUTHORIZATION= "Authorization";

    public static final int PROPERTIES_CACHESIZE = 4 * 1024 * 1024; // 4MiB

    /*TAG*/
        //CLASSNAME
        public static final String TAG_CLASSNAME_FLICKR = "FLICKR";
        public static final String TAG_CLASSNAME_MAINACTIVITY = "MAINACTIVITY";
        public static final String TAG_CLASSNAME_MYADAPTER = "MYADAPTER";
        public static final String TAG_CLASSNAME_AUTHENTICATION = "AUTHENTICATION";
        public static final String TAG_CLASSNAME_ASYNCFLICKRJSONDATA = "ASYNCFLICKRJSONDATA";
        public static final String TAG_CLASSNAME_ASYNCBITMAPDOWNLOADER = "ASYNCBITMAPDOWNLOADER";
        public static final String TAG_CLASSNAME_ASYNCFLICKRJSONDATAFORLIST = "ASYNCFLICKRJSONDATAFORLIST";
        public static final String TAG_CLASSNAME_THREADCUSTOM = "THREADCUSTOM";
        public static final String TAG_CLASSNAME_HELPER = "HELPER";
        public static final String TAG_CLASSNAME_ASYNCFLICKRJSONDATAADVANCED = "ASYNCFLICKRJSONDATAADVANCED";

        //METHODS & FUNCTIONS
        public static final String TAG_METFUNC_ONCLICK= "ONCLICK";
        public static final String TAG_METFUNC_REDIRECTACTIVITY= "REDIRECTACTIVITY";
        public static final String TAG_METFUNC_ADDURL= "ADDURL";
        public static final String TAG_METFUNC_DOINBACKGROUND= "DOINBACKGROUND";
        public static final String TAG_METFUNC_ONPOSTEXECUTE= "ONPOSTEXECUTE";
        public static final String TAG_METFUNC_READSTREAM= "READSTREAM";
        public static final String TAG_METFUNC_REQUESTACTION= "REQUESTACTION";

    /*JSON TOKEN*/
        public static final String JSONTOKEN_ITEMS = "items";
        public static final String JSONTOKEN_MEDIA = "media";
        public static final String JSONTOKEN_URL = "m";
        public static final String JSONTOKEN_AUTHENTICATED = "authenticated";
        public static final String JSONTOKEN_SERVER = "server";
        public static final String JSONTOKEN_ID = "id";
        public static final String JSONTOKEN_PHOTOS = "photos";
        public static final String JSONTOKEN_PHOTO = "photo";
        public static final String JSONTOKEN_SECRET = "secret";

    /*MSG*/
        //INFO
        public static final String MSG_INFO_DEFAULTCASE = "Default case, to prevent unwanted cases";
        public static final String MSG_INFO_ADDEDURL = "Added url";
        //ERROR
        public static final String MSG_ERROR_WRONGDATA = "Wrong data given or no data given";
        public static final String MSG_ERROR_EMPTYFIELDS = "The fields must be filled";
}

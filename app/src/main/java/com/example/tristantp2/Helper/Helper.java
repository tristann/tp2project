package com.example.tristantp2.Helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.example.tristantp2.Utils.Constants.*;

/**
 * This class is used to avoid the repetition of some functions or methods in the program.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class Helper {

    /**
     * This function is used to read the input and to translate the input into a String to be processed.
     *
     * @param is it is the input that we want to read and to convert to a String
     * @return String return a string representation of the input given
     */
    public static String ReadStream(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_HELPER,TAG_METFUNC_READSTREAM), e.getMessage(), e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_HELPER,TAG_METFUNC_READSTREAM), e.getMessage(), e);
            }
        }
        return sb.toString();
    }

    /**
     * This function is used to check if the permissions that we need to ask for the use are granted or not.
     *
     * @param context it is the context
     * @return boolean return true if the permission are not yet accepted by the user
     */
    public static boolean checkPermissionGranted(Context context){
        boolean fine_location = ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;
        boolean coarse_location =  ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;

        return (fine_location && coarse_location);
    }

    /**
     * This method is used to ask for the permissions to use some permissions.
     *
     * @param activity it is the activity
     */
    public static void askPermissions(Activity activity){
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
        ActivityCompat.requestPermissions(activity, permissions, 0);
    }
}

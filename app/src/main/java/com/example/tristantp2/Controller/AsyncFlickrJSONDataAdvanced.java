package com.example.tristantp2.Controller;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.tristantp2.Utils.Constants.*;
import static com.example.tristantp2.Helper.Helper.*;

/**
 * This class is used to get an image from your location with an url.
 * The link of the image will be send to another class to be downloaded and displayed.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class AsyncFlickrJSONDataAdvanced extends AsyncTask<String,Void, JSONObject> {
    private ImageView img_getFlickr;

    /**
     * Constructor of AsyncFlickrJSONDataAdvanced
     * @param img_getFlickr ImageView that we want to update/set.
     */
    public AsyncFlickrJSONDataAdvanced(ImageView img_getFlickr) {
        this.img_getFlickr = img_getFlickr;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        JSONObject objJ = null;
        URL url = null;
        try {
            url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String s = ReadStream(in);
                objJ = new JSONObject(s);
                objJ = objJ.getJSONObject(JSONTOKEN_PHOTOS);
            } catch (JSONException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATAADVANCED,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATAADVANCED,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATAADVANCED,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        }
        return objJ;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        try {
            JSONObject objectImg = jsonObject.getJSONArray(JSONTOKEN_PHOTO).getJSONObject(0);
            new AsyncBitmapDownloader(img_getFlickr).execute(String.format("%s%s/%s_%s.jpg",PROPERTIES_API_URL_PHOTO,objectImg.getString(JSONTOKEN_SERVER),objectImg.getString(JSONTOKEN_ID),objectImg.getString(JSONTOKEN_SECRET)));
        } catch (JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATAADVANCED,TAG_METFUNC_ONPOSTEXECUTE), e.getMessage(), e);
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(JSONObject jsonObject) {
        super.onCancelled(jsonObject);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

}

package com.example.tristantp2.Controller;

import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.tristantp2.Helper.Helper.*;
import static com.example.tristantp2.Utils.Constants.*;


/**
 * This class is used to authenticate a user by asking an API.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class ThreadCustom extends Thread{
    private String login ,password;
    private TextView lbl_result;

    /**
     * Constructor of ThreadCustom
     * @param name name of the thread
     */
    public ThreadCustom(String name){
        super(name);
        login = PROPERTIES_DEFAULT_STRING;
        password = PROPERTIES_DEFAULT_STRING;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TextView getLbl_result() {
        return lbl_result;
    }

    public void setLbl_result(TextView lbl_result) {
        this.lbl_result = lbl_result;
    }

    @Override
    public void run() {
        requestAction();
    }

    /**
     * It is the method which represent what the thread will do.
     * This method will use the password and the login given to try an authentication.
     *
     * @return void
     */
    private void requestAction(){
        URL url = null;
        try {
            url = new URL(PROPERTIES_API_URL_AUHT);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestProperty (PROPERTIES_AUTHORIZATION, String.format("Basic %s",Base64.encodeToString(String.format("%s:%s",login,password).getBytes(), Base64.NO_WRAP)));
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String s = ReadStream(in);
                JSONObject objJ = new JSONObject(s);
                writeResult(objJ.getString(JSONTOKEN_AUTHENTICATED));
            } catch (JSONException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_THREADCUSTOM,TAG_METFUNC_REQUESTACTION), e.getMessage(), e);
            } catch (FileNotFoundException e) {
                writeResult(PROPERTIES_FALSE);
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_THREADCUSTOM,TAG_METFUNC_REQUESTACTION), e.getMessage(), e);
            }finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_THREADCUSTOM,TAG_METFUNC_REQUESTACTION), e.getMessage(), e);
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_THREADCUSTOM,TAG_METFUNC_REQUESTACTION), e.getMessage(), e);
        }
    }

    /**
     * This method is used to display a message by requesting the UI Thread to update a particular
     * component. Here it is the label representing the state of the authentication.
     *
     * @param result It is a message which will be displayed on the screen. It is the result of the authentication part.
     * @return void
     */

    private void writeResult(final String result){
        lbl_result.post(new Runnable() {
            @Override
            public void run() {
                lbl_result.setText(String.format("Auth = [%s]",result));
            }
        });
    }
}

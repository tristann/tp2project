package com.example.tristantp2.Controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.tristantp2.R;

import static com.example.tristantp2.Utils.Constants.*;

/**
 * This class is used to redirect the user to either the Flickr part
 * or the authenticate part. It is the entry point activity.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_Auth;
    private Button btn_Flick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*GET COMPONENT*/
        btn_Auth = (Button) findViewById(R.id.btn_auth);
        btn_Flick = (Button) findViewById(R.id.btn_flickr);

        /*ADD LISTENER*/
        btn_Auth.setOnClickListener(this);
        btn_Flick.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_auth:
                redirectionActivity(PROPERTIES_CASE_AUTH);
                break;
            case R.id.btn_flickr:
                redirectionActivity(PROPERTIES_CASE_FLICKR);
                break;
            default:
                Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_MAINACTIVITY,TAG_METFUNC_ONCLICK),MSG_INFO_DEFAULTCASE);
        }
    }

    /*FUNCTION*/
    /**
     * This method is used to redirect the user according to a keyword.
     *
     * @param target its represent the part of the lab we need to be redirected.
     * @return void
     */
    private void redirectionActivity(String target){
        Intent intent;
        boolean auth = !target.toLowerCase().equals(PROPERTIES_CASE_AUTH.toLowerCase());
        boolean flickr = !target.toLowerCase().equals(PROPERTIES_CASE_FLICKR.toLowerCase());

        if(target.isEmpty() && (auth || flickr )){
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_MAINACTIVITY,TAG_METFUNC_REDIRECTACTIVITY),MSG_ERROR_WRONGDATA);
            return;
        }
        switch (target.toLowerCase()){
            case PROPERTIES_CASE_AUTH:
                intent = new Intent(this,Authentication.class);
                startActivity(intent);
                break;
            case PROPERTIES_CASE_FLICKR:
                intent = new Intent(this,Flickr.class);
                startActivity(intent);
                break;
            default:
                Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_MAINACTIVITY,TAG_METFUNC_REDIRECTACTIVITY),MSG_INFO_DEFAULTCASE);
        }
    }
}
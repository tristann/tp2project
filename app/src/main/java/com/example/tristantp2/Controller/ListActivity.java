package com.example.tristantp2.Controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.tristantp2.R;

import static com.example.tristantp2.Utils.Constants.*;

/**
 * This class is used to display a list of images from an API.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class ListActivity extends AppCompatActivity {

    private ListView list_root;
    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        /*GET COMPONENT*/
        list_root = (ListView) findViewById(R.id.list_root);

        /*ADD LISTENER*/

        /*OTHER*/
        adapter = new MyAdapter(this);
        list_root.setAdapter(adapter);

        new AsyncFlickrJSONDataForList(adapter).execute(PROPERTIES_API_URL);
    }

    /*FUNCTION*/
}
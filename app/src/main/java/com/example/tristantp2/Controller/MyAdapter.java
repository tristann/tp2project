package com.example.tristantp2.Controller;


import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.example.tristantp2.Model.MySingleton;
import com.example.tristantp2.R;

import java.util.Vector;

import static com.example.tristantp2.Utils.Constants.*;

/**
 * This class is used to inflate the listview to display a list of images.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class MyAdapter extends BaseAdapter {

    private Vector<String> vector = new Vector<String>();
    private Context context;

    /**
     * Constructor of MyAdapter
     * @param context context used to be inflated
     */
    public MyAdapter(Context context) {
        this.context = context;
    }


    /**
     * This method is used to add a link given to the vector containing all the images links.
     *
     * @param url It is the link of an image.
     * @return void
     */
    public void addUrl(String url){
        vector.add(url);
        Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_MYADAPTER,TAG_METFUNC_ADDURL),String.format("%s (%s)", MSG_INFO_ADDEDURL,url));
    }

    @Override
    public int getCount() {
        return vector.size();
    }

    @Override
    public Object getItem(int position) {
        return vector.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        /*[EXERCISE 25]
        View view = LayoutInflater.from(context).inflate(R.layout.row_layout, parent,false);

        LinearLayout ln_item = view.findViewById(R.id.ln_item);
        TextView lbl_url_item = view.findViewById(R.id.lbl_url_item);

        lbl_url_item.setText(vector.get(position));
        */

        View view = LayoutInflater.from(context).inflate(R.layout.bitmaplayout, parent,false);

        LinearLayout ln_item = view.findViewById(R.id.ln_item_bitmap);
        final ImageView img_api_item = view.findViewById(R.id.img_api_item);

        RequestQueue queue = MySingleton.getInstance(context).getRequestQueue();

        ImageRequest imageRequest = new ImageRequest(vector.get(position),
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        img_api_item.setImageBitmap(response);
                    }
                },
                0,
                0,
                ImageView.ScaleType.CENTER_CROP,
                Bitmap.Config.RGB_565,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();
                        Toast.makeText(context,error.getMessage(),Toast.LENGTH_LONG);
                    }
                }
        );

        queue.add(imageRequest);

        return view;

    }
}

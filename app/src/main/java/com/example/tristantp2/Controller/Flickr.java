package com.example.tristantp2.Controller;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.tristantp2.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import static com.example.tristantp2.Utils.Constants.*;
import static com.example.tristantp2.Helper.Helper.*;

/**
 * This class is used to display a list of images, to display an image from your location
 * and to display the first images with the tag TREE.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class Flickr extends AppCompatActivity implements View.OnClickListener {

    private Button btn_getImg, btn_list, btn_getImg_Loc;
    private ImageView img_flickr;
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flickr);

        /*GET COMPONENT*/
        btn_getImg = (Button) findViewById(R.id.btn_getImg);
        btn_list = (Button) findViewById(R.id.btn_list);
        btn_getImg_Loc = (Button) findViewById(R.id.btn_getImg_Loc);
        img_flickr = (ImageView) findViewById(R.id.img_flickr);

        /*ADD LISTENER*/
        btn_getImg.setOnClickListener(this);
        btn_list.setOnClickListener(this);
        btn_getImg_Loc.setOnClickListener(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_getImg:
                new AsyncFlickrJSONData(img_flickr).execute(PROPERTIES_API_URL);
                break;
            case R.id.btn_list:
                Intent intent = new Intent(this, ListActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_getImg_Loc:
                if(!checkPermissionGranted(this)){
                    fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                new AsyncFlickrJSONDataAdvanced(img_flickr).execute(String.format("%s&lat=%s&lon=%s",PROPERTIES_API_URL_ADVANCED,location.getLatitude(),location.getLongitude()));
                            }
                        }
                    });
                }else{
                    askPermissions(this);
                }
                break;
            default:
                Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_FLICKR,TAG_METFUNC_ONCLICK),MSG_INFO_DEFAULTCASE);
        }
    }

    /*FUNCTION*/
}
package com.example.tristantp2.Controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tristantp2.R;

import static com.example.tristantp2.Utils.Constants.*;

/**
 * This class is used to authenticate a user with a login and a password.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class Authentication extends AppCompatActivity implements View.OnClickListener {

    private EditText edt_password,edt_login;
    private Button btn_authenticate;
    private TextView lbl_login,lbl_password,lbl_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        /*GET COMPONENT*/
        edt_password = (EditText) findViewById(R.id.edt_password);
        edt_login = (EditText) findViewById(R.id.edt_login);
        btn_authenticate = (Button) findViewById(R.id.btn_authenticate);
        lbl_login = (TextView) findViewById(R.id.lbl_login);
        lbl_password = (TextView) findViewById(R.id.lbl_password);
        lbl_result = (TextView) findViewById(R.id.lbl_result);

        /*ADD LISTENER*/
        btn_authenticate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_authenticate:
                if(isValidField()){
                    lbl_result.setText(PROPERTIES_EMPTY_STRING);
                    ThreadCustom thread = new ThreadCustom(PROPERTIES_THREAD_THREADCUSTOM);
                    thread.setLogin(edt_login.getText().toString().toLowerCase());
                    thread.setPassword(edt_password.getText().toString().toLowerCase());
                    thread.setLbl_result(lbl_result);
                    thread.start();
                }else{
                    lbl_result.setText(MSG_ERROR_EMPTYFIELDS);
                }
                break;
            default:
                Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_AUTHENTICATION,TAG_METFUNC_ONCLICK),MSG_INFO_DEFAULTCASE);
        }
    }

    /*FUNCTION*/
    /**
     * This function is used to verify if the inputs are valid or not.
     *
     * @return boolean return True if the fields are both filled.
     */
    private boolean isValidField(){
        if(edt_login.getText().toString().equals(PROPERTIES_EMPTY_STRING) || edt_password.getText().toString().equals(PROPERTIES_EMPTY_STRING)){
            return false;
        }
        return true;
    }
}
package com.example.tristantp2.Controller;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.tristantp2.Utils.Constants.*;
import static com.example.tristantp2.Helper.Helper.*;

/**
 * This class is used to get a list of images link from the Flickr API.
 * After that those link are added to the adapter to be displayed.
 *
 * @author  Tristan CLEMENCEAU
 * @since   2021-02-26
 */

public class AsyncFlickrJSONDataForList extends AsyncTask<String,Void, JSONObject> {

    private MyAdapter adapter;

    /**
     * Constructor of AsyncFlickrJSONDataForList
     * @param adapter adapter to send the images links
     */
    public AsyncFlickrJSONDataForList(MyAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        JSONObject objJ = null;
        URL url = null;
        try {
            url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String s = ReadStream(in);
                objJ = new JSONObject(s);
            } catch (JSONException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATAFORLIST,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATAFORLIST,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATAFORLIST,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        }
        return objJ;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        try {
            JSONArray arrayObj = jsonObject.getJSONArray(JSONTOKEN_ITEMS);
            for(int i=0;i<arrayObj.length();i++)
            {
                adapter.addUrl(arrayObj.getJSONObject(i).getJSONObject(JSONTOKEN_MEDIA).getString(JSONTOKEN_URL));
            }

            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCFLICKRJSONDATAFORLIST,TAG_METFUNC_ONPOSTEXECUTE), e.getMessage(), e);
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(JSONObject jsonObject) {
        super.onCancelled(jsonObject);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

}
